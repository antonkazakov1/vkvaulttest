package com.antonkazakov.vkvault.screens.files_screen.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.antonkazakov.vkvault.R;
import com.antonkazakov.vkvault.models.docs.get.DocListItem;
import com.antonkazakov.vkvault.screens.files_screen.presenters.DocsPresenterImpl;
import com.antonkazakov.vkvault.screens.files_screen.views.DocsView;
import com.antonkazakov.vkvault.utils.recyclerstuff.ClickListener;
import com.antonkazakov.vkvault.utils.recyclerstuff.DividerItemDecoration;
import com.antonkazakov.vkvault.utils.recyclerstuff.RecyclerTouchListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DocsFragment extends Fragment implements DocsView{

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;


    DocsAdapter docsAdapter;

    DocsPresenterImpl docsPresenter;

    List<DocListItem> docListItems = new ArrayList<>();

    private BottomSheetBehavior mBottomSheetBehavior;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_docs, container, false);
        ButterKnife.bind(this,view);

        initRecyclerView();
        docsPresenter.getDocuments();
        View bottomSheet = view.findViewById( R.id.bottom_sheet );
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        docsPresenter = new DocsPresenterImpl(this);
        docsAdapter = new DocsAdapter(docListItems);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void initRecyclerView(){

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark);
        swipeRefreshLayout.setOnRefreshListener(() -> docsPresenter.getDocuments());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(docsAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                docsPresenter.getDocument(docListItems.get(position).getId());
            }

            @Override
            public void onLongClick(View view, int position) {
                showMenu(position);
            }

            @Override
            public void onDoubleClick(View view, int position) {

            }
        }));

    }


    @Override
    public void getDocs(List<DocListItem> docListItems1) {
        docListItems.clear();
        docListItems.addAll(docListItems1);
        docsAdapter.notifyDataSetChanged();
    }

    @Override
    public void getDoc(DocListItem docItem) {
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
       // tv_name.setText(docItem.getTitle());
       // tv_size.setText(docItem.getSize()+" байт");
       // tv_date.setText(docItem.getDate());
    }

    @Override
    public void onError() {

    }

    @Override
    public void onNoConnectionError() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void loadByName(String name){
        docsPresenter.getDocsByName(name);
    }


    private void showEditDialog(int id){
        new MaterialDialog.Builder(getActivity())
                .title("Переименовать файл")
                .inputType(InputType.TYPE_CLASS_TEXT )
                .inputMaxLength(128)
                .input("Новое название", null, (dialog, input) -> docsPresenter.editDocument(docListItems.get(id).getId(),input.toString()))
                .positiveColor(ContextCompat.getColorStateList(getActivity(),R.color.colorPrimary))
                .negativeColor(ContextCompat.getColorStateList(getActivity(),R.color.colorGry))
                .onNegative((dialog, which) -> dialog.dismiss())
                .negativeText("Отмена")
                .positiveText("ОК")
                .show();
    }

    private void showDeleteDialog(int id){
        new MaterialDialog.Builder(getActivity())
                .title("Удалить файл")
                .content("Удалить файл?")
                .negativeText("Отмена")
                .positiveColor(ContextCompat.getColorStateList(getActivity(),R.color.colorPrimary))
                .negativeColor(ContextCompat.getColorStateList(getActivity(),R.color.colorGry))
                .positiveText("Удалить")
                .onNegative((dialog, which) -> dialog.dismiss())
                .onPositive((dialog, which) -> docsPresenter.deleteDocument(docListItems.get(id).getId()))
                .show();
    }


    private void showMenu(int id){
        new MaterialDialog.Builder(getActivity())
                .positiveColor(ContextCompat.getColorStateList(getActivity(),R.color.colorPrimary))
                .negativeColor(ContextCompat.getColorStateList(getActivity(),R.color.colorGry))
                .title("Меню")
                .items("Переименовать файл"," Удалить файл")
                .itemsCallback((dialog, view, which, text) -> {
                  switch (which){
                      case 0:
                          showEditDialog(id);
                          break;
                      case 1:
                          showDeleteDialog(id);
                          break;
                  }
                })
                .show();
    }

}
