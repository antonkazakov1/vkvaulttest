package com.antonkazakov.vkvault.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by antonkazakov on 24.09.16.
 */

public class SimpleResponse {

    @SerializedName("response")
    @Expose
    private Integer response;

    /**
     *
     * @return
     * The response
     */
    public Integer getResponse() {
        return response;
    }

    /**
     *
     * @param response
     * The response
     */
    public void setResponse(Integer response) {
        this.response = response;
    }

}
