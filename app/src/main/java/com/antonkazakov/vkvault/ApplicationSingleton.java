package com.antonkazakov.vkvault;

import android.app.Application;
import android.content.Context;


import com.vk.sdk.VKSdk;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by antonkazakov on 19.09.16.
 */
public class ApplicationSingleton  extends Application {

    public static ApplicationSingleton applicationSingleton;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationSingleton=this;
        VKSdk.initialize(getApplicationContext());
        initRealm();
    }

    public static ApplicationSingleton getContext() {
        return applicationSingleton;
    }

    public void initRealm(){
        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext())
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(2)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }


}
